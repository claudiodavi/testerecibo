/* INICIANDO SQLITE */
var db = window.openDatabase("database", "1.0", "recibos", 65536);
var meuid = false;
var meuemail = false;
var minhahash = false;
var cont = 0;

db.transaction(function(tx) {
    var query = 'CREATE TABLE IF NOT EXISTS usuario(id INTEGER NOT NULL PRIMARY KEY, nome VARCHAR NOT NULL, email VARCHAR NOT NULL, hash VARCHAR NOT NULL);';
    tx.executeSql(query, []);
});

db.transaction(function(tx) {
    var query = 'CREATE TABLE IF NOT EXISTS recibos(id INTEGER NOT NULL PRIMARY KEY, idEmissor INTEGER NOT NULL, idReceptor INTEGET NOT NULL, servico VARCHAR NOT NULL, valor FLOAT NOT NULL, data VARCHAR NOT NULL);';
    tx.executeSql(query, []);
});

/* espera carregar (init) a pagina list.html */
document.addEventListener("init", function(event) {

  /*quando abrir a tela index - tela inicial
   * testa e ja existe alguem no banco - logado
   * se nao existe alguem no banco manda para a tela de login/registro
   */
    if (event.target.id == "index.html") {
        db.transaction(function(tr) {
            tr.executeSql("SELECT ID FROM usuario", [], function(trq, results) { //CALL BACK sucess
                if (results.rows.length == 1) { //se tiver alguem no banco entao loga direto
                    toList();
                } else { //senao vai pra tela pra logar/registrar
                    toLogin();
                }
            });
        });
    }

    /* quando abrir a tela de list funcao de mostrar a lista*/
    if (event.target.id == "emitidos.html") {
        mostrarListaEmitidos(document.getElementById("infinite-list-emitidos"));
    }

    /* quando abrir a tela de list - recebidos -  chama a funcao de mostrar a lista*/
    if (event.target.id == "recebidos.html") {
        mostrarListaRecebidos(document.getElementById("infinite-list-recebidos"));
    }


}, false);
/*fim da addEventListener*/


/*login
 * recebe um json com os dados para o login
 * coloca cada campo em uma variavel
 * cria uma query para inserir no banco o usuario que estao logado
 * salva nas variaveis globais o id, email e a hash do usuario logado
 */
function iniciarSessao(json) {
    var id = json['id'];
    var nome = json['nome'];
    var email = json['email'];
    var hash = json['hash'];
    db.transaction(function(tr) {
        var query = "INSERT INTO usuario (id, nome, email, hash) VALUES (?, ?, ?, ?);";
        tr.executeSql(query, [id, nome, email, hash], function() {
            meuid = id;
            meuemail = email;
            minhahash = hash;
            toList();
        }, function(a, b) { //callback de erro
            console.log(a, b);
        });
    });
};

/* logout
 * deleta tudo da tabela recibos
 * deleta tudo da tabela usuario - como callback de sucesso de ter tirado os recibos
 * callback de sucesso quando sai do usuario eh levar ate a tela de login
 */
function encerrarSessao() {
    db.transaction(function(tr) {
        var query = "DELETE FROM recibos";
        tr.executeSql(query, [], function(tx) { //callback sucesso para sair os recibos
            var query = "DELETE FROM usuario";
            tx.executeSql(query, [], function(tx) { //callback sucesso para sair usuario
                insertPage(1, "login.html");
            }, function(a, b) {
                ons.notification.alert('Erro ao deslogar');
            });
        }, function(a, b) { //callback de erro ao sair os recibos
            ons.notification.alert('Erro ao deslogar');
        });
    }); //fecha transaction
};

/* sincronizarDados
 * recebe um json com user_recibos e user_info
 * coloca tudo de user_recibos nas variaveis
 * cria a query para inserir na tabela recibos e insere
 * entao inicia a sessao (loga o usuario)
 * caso o usuario nao possua nenhum recibo, loga direto
 */
function sincronizarDados(json) {
    db.transaction(function(tx) {
        if (json['user_recibos'].length > 0) {
            for (var i in json['user_recibos']) {
                var id = parseInt(json['user_recibos'][i]['id']);
                var idReceptor = parseInt(json['user_recibos'][i]['receptorid']);
                var idEmissor = parseInt(json['user_recibos'][i]['emissorid']);
                var servico = json['user_recibos'][i]['servico'];
                var valor = parseFloat(json['user_recibos'][i]['valor']);
                var data = json['user_recibos'][i]['data'];
                var query = "INSERT INTO recibos (id, idEmissor, idReceptor, servico, valor, data) VALUES (?, ?, ?, ?, ?, ?);";
                tx.executeSql(query, [id, idEmissor, idReceptor, servico, valor, data]);
            }
            iniciarSessao(json['user_info']);
        } else {
            iniciarSessao(json['user_info']);
        }
    });

};


/* cria de fato a lista infinita - emitidos*/
function mostrarListaEmitidos(infiniteList) {
    db.transaction(function(tr) {
        tr.executeSql("select * from recibos WHERE idEmissor=?", [meuid], function(trq, results) { //pega todos os recibos
            infiniteList.delegate = {
                createItemContent: function(i) {
                    return ons._util.createElement(
                        '<ons-list-item tappable="true" onclick="mostrarDados(' + results.rows.item(i).id + ')"> ' + results.rows.item(i).id + ' ' + results.rows.item(i).servico +
                        ' - para ' + results.rows.item(i).idReceptor + '</ons-list-item>'
                    );
                },
                countItems: function() {
                    return (results.rows.length);
                },
                calculateItemHeight: function() {
                    return ons.platform.isAndroid() ? 48 : 44;
                }
            };
        });

    });
}

/* cria de fato a lista infinita - recebidos*/
function mostrarListaRecebidos(infiniteList) {
    db.transaction(function(tr) {
        tr.executeSql("SELECT * FROM recibos WHERE idReceptor=?", [meuid], function(trq, results) { //pega todos os recibos
            infiniteList.delegate = {
                createItemContent: function(i) {
                    return ons._util.createElement(
                        '<ons-list-item tappable="true" onclick="mostrarDados(' + results.rows.item(i).id + ')"> ' + results.rows.item(i).id + ' ' + results.rows.item(i).servico +
                        ' - para ' + results.rows.item(i).idReceptor + '</ons-list-item>'
                    );
                },
                countItems: function() {
                    return (results.rows.length);
                },
                calculateItemHeight: function() {
                    return ons.platform.isAndroid() ? 48 : 44;
                }
            };
        });

    });

}


function mostrarDados(i) {
    db.transaction(function(tr) {
        tr.executeSql('SELECT * FROM recibos WHERE id=? LIMIT 1', [i], function(trq, results) { //pega todos os recibos
            var row = results.rows.item(0);
            alert("Id do Emissor: " + (row.idEmissor == meuid ? row.idEmissor + '(eu)' : row.idEmissor) + "\n" +
                "\nId Receptor: " + (row.idReceptor == meuid ? row.idReceptor + '(eu)' : row.idReceptor) + "\n" +
                "\nServiço: " + row.servico + "\nValor: R$" + row.valor + "\nData: " + row.data);
        });
    });
}


function cadastrar() {
    var usuario = new Usuario();
    var validado = usuario.validaUsuario();
    var ids = ['btn-back-cadastro'];
    var idDiv = 'esperaCadastro';
    var html = "<ons-button id='btn-registrar' modifier='large' onclick='cadastrar()'><ons-ripple></ons-ripple>Registrar-se</ons-button>";
    switch (validado) {
        case 0:
            ons.notification.alert("Certifique-se de que preencher corretamente todos os campos!");
            break;
        case 1:
            ons.notification.alert("As senhas não coincidem!");
            document.getElementById('senhaUsuario').value = "";
            document.getElementById('senhaUsuario2').value = "";
            break;
        default:
            /* CADASTRAR AQ*/
            ligarAnimacao(ids, idDiv);
            xhr = new XMLHttpRequest();
            var url = "https://dev.certificados3s.com/recibos3s/communication/?json=" + encodeURIComponent(JSON.stringify({
                "token": "123",
                "action": "register",
                "email": usuario.getEmail(),
                "senha": usuario.getSenha(),
                "cpf": usuario.getcpf(),
                "nome": usuario.getNome()
            }));
            xhr.open("GET", url, true);
            xhr.setRequestHeader("Content-type", "application/json");
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    var json = JSON.parse(xhr.responseText);
                    if (json['retorno']) {
                        ons.notification.alert(json['mensagem']);
                        /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
                        desligarAnimacao(ids, idDiv, html);
                        console.log(json);
                        iniciarSessao(json['dadosRetorno']['user_info']);
                        sincronizarRecibos(json['dadosRetorno']['user_recibos']);
                    } else {
                        desligarAnimacao(ids, idDiv, html);
                        ons.notification.alert(json['mensagem']);
                    }
                }
                xhr.timeout = 10000;
                xhr.ontimeout = function() {
                    desligarAnimacao(ids, idDiv, html);
                    ons.notification.alert("Desculpe-nos! Estamos com problemas internos. Aguarde um momento e tente novamente!");
                };
            };
            xhr.timeout = 10000;
            xhr.ontimeout = function() {
                desligarAnimacao(ids, idDiv, html);
                ons.notification.alert("Desculpe-nos! Estamos com problemas internos. Aguarde um momento e tente novamente!");
            };
            xhr.setRequestHeader("Authorization", "Basic " + btoa('3s:3s9961teste'));
            xhr.send();
    }
};

function gerarReciboRapido() {
    var receptor = new Receptor();
    var validado = receptor.testeVazio();
    var ids = ['voltar', 'btn-confirmaDados'];
    var idDiv = 'esperaReciboRapido'
    var html = "<ons-button id='btn-confirmaDados' modifier='large' onclick='novoEmissor()'><ons-ripple></ons-ripple>Confirmar dados</ons-button>";
    if (!validado) {
        ligarAnimacao(ids, idDiv);
        xhr = new XMLHttpRequest();
        var url = "https://dev.certificados3s.com/recibos3s/communication/?json=" + encodeURIComponent(JSON.stringify({
            "token": "123",
            "action": "novoRecibo",
            "emissornome": emissor.getNome(),
            "emissoremail": emissor.getEmail(),
            "receptornome": receptor.getNome(),
            "receptoremail": receptor.getEmail(),
            "servico": emissor.getServico(),
            "valor": emissor.getValor()
        }));
        xhr.open("GET", url, true);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                var json = JSON.parse(xhr.responseText);
                if (json['retorno']) {
                    ons.notification.alert(json['mensagem']);
                    document.getElementById('nomeEmissor').value = "";
                    document.getElementById('emailEmissor').value = "";
                    document.getElementById('servicoRapido').value = "";
                    document.getElementById('valorRapido').value = "";
                    document.getElementById('nomeReceptor').value = "";
                    document.getElementById('emailReceptor').value = "";
                    toLogin();

                }
            } // fim do if do xhr
              xhr.timeout = 10000;
              xhr.ontimeout = function() {
                  /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
                  desligarAnimacao(ids, idDiv, html);
                  ons.notification.alert("Desculpe-nos! Limite de tempo de resposta do servidor excedido. Aguarde um momento e tente novamente!");
              };

        }; // fim da onreadystatechange()

        xhr.timeout = 10000;
        xhr.ontimeout = function() {
            /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
            desligarAnimacao(ids, idDiv, html);
            ons.notification.alert("Desculpe-nos! Limite de tempo de resposta do servidor excedido. Aguarde um momento e tente novamente!");
        };
        xhr.setRequestHeader("Authorization", "Basic " + btoa('3s:3s9961teste'));
        xhr.send();

    } else { //else do validado
        ons.notification.alert("Certifique-se de que preencheu corretamente todos os campos!");
    }
}

function gerarReciboLogado() {
    var recibo = new reciboLogado();
    var ids = ['btn-gerarrecibolog'];
    var idDiv = 'esperaRecibo';
    var html = "<ons-button id='btn-gerarecibolog' onclick='gerarReciboLogado()' class='button button--large'>Confirmar Dados</ons-button>";
    recibo.validaReciboLogado();
    ligarAnimacao(ids, idDiv);
    xhr = new XMLHttpRequest();
    var url = "https://dev.certificados3s.com/recibos3s/communication/?json=" + encodeURIComponent(JSON.stringify({
        "token": "123",
        "usertoken": minhahash, //window.btoa(meuid + meuemail + minhahash),
        "action": "novoRecibo",
        "id": meuid,
        "receptornome": recibo.getNome(),
        "receptoremail": recibo.getEmail(),
        "servico": recibo.getServico(),
        "valor": recibo.getValor()
    }));
    xhr.open("GET", url, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            if (json['retorno']) {
                db.transaction(function(tx) {
                    var id = parseInt(json['dadosRetorno']['id']);
                    var idEmissor = parseInt(json['dadosRetorno']['emissorid']);
                    var idReceptor = parseInt(json['dadosRetorno']['receptorid']);
                    var servico = json['dadosRetorno']['servico'];
                    var valor = parseFloat(json['dadosRetorno']['valor']);
                    var data = json['dadosRetorno']['data'];
                    var query = "INSERT INTO recibos (id, idEmissor, idReceptor, servico, valor, data) VALUES (?, ?, ?, ?, ?, ?);";
                    tx.executeSql(query, [id, idEmissor, idReceptor, servico, valor, data], function sucesso() {
                        ons.notification.alert(json['mensagem']);
                        desligarAnimacao(ids, idDiv, html);
                    }, function erro(a, b) {
                        ons.notification.alertt(json['mensagem']);
                        desligarAnimacao(ids, idDiv, html);
                    });
                });
            }
            xhr.timeout = 10000;
            xhr.ontimeout = function() {
                /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
                desligarAnimacao(ids, idDiv, html);
                ons.notification.alert("Desculpe-nos! Limite de tempo de resposta do servidor excedido. Aguarde um momento e tente novamente!");

            };

        }
    };
    xhr.timeout = 10000;
    xhr.ontimeout = function() {
        /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
        desligarAnimacao(ids, idDiv, html);
        ons.notification.alert("Desculpe-nos! Limite de tempo de resposta do servidor excedido. Aguarde um momento e tente novamente!");

    };
    xhr.setRequestHeader("Authorization", "Basic " + btoa('3s:3s9961teste'));
    xhr.send();

}

/* FUNCAO DE LOGIN */
function logar() {
    var email = document.getElementById('emailUsuarioLogin').value;
    var senha = document.getElementById('senhaUsuarioLogin').value;
    var cont = 0;
    var ids = ['btn-esqueci', 'btn-inscrevase', 'btn-gerarecibo'];
    var idDiv = 'espera';
    var html = "<ons-button id='btn-login' modifier='large--cta' onclick='logar()'><ons-ripple></ons-ripple>Entrar</ons-button>";
    if (!senha || !email) {
        ons.notification.alert("Certifique-se de que preencheu corretamente todos os campos!");
    } else {
        ligarAnimacao(ids, idDiv);
        xhr = new XMLHttpRequest();
        var url = "https://dev.certificados3s.com/recibos3s/communication/?json=" + encodeURIComponent(JSON.stringify({
            "token": "123",
            "action": "login",
            "email": email,
            "senha": senha
        }));
        xhr.open("GET", url, true);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && xhr.status == 200) {
                var json = JSON.parse(xhr.responseText);
                if (json['retorno']) {
                    /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
                    desligarAnimacao(ids, idDiv, html);
                    /*muda para a pagina de login*/
                    if (cont == 0) {
                        sincronizarDados(json['dadosRetorno']);
                        cont++;
                    }
                } else {
                    /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
                    desligarAnimacao(ids, idDiv, html);
                    ons.notification.alert(json['mensagem']);
                }
            }

            xhr.timeout = 10000;
            xhr.ontimeout = function() {
                /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
                desligarAnimacao(ids, idDiv, html);
                ons.notification.alert("Desculpe-nos! Limite de tempo de resposta do servidor excedido. Aguarde um momento e tente novamente!");
            };
        };

        xhr.timeout = 10000;
        xhr.ontimeout = function() {
            /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
            desligarAnimacao(ids, idDiv, html);
            ons.notification.alert("Desculpe-nos! Limite de tempo de resposta do servidor excedido. Aguarde um momento e tente novamente!");
        };
        xhr.setRequestHeader("Authorization", "Basic " + btoa('3s:3s9961teste'));
        xhr.send();
    }

}
/* FIM FUNCAO DE LOGIN */
function logout() {
    ons.notification.confirm({
        message: 'Você realmente deseja sair?',
        callback: function(idx) {
            switch (idx) {
                case 0:
                    break;
                case 1:
                    document
                        .getElementById('dialogo-saida')
                        .show();

                    encerrarSessao();
                    break;
            }
        }
    });

};
