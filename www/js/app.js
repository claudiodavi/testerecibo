window.fn = {};

window.fn.open = function() {
    var menu = document.getElementById('menu');
    menu.open();
};

window.fn.load = function(page) {
    var content = document.getElementById('content');
    var menu = document.getElementById('menu');
    content.load(page)
        .then(menu.close.bind(menu));
};

/* EMISSOR OBJETO GLOGAL */
var emissor = new Object();

/* FUNCOES PARA NAVEGACAO NAS PAGINAS */

function insertPage(index, page){
  document.getElementById('nav1').insertPage(index, page);
}
function pop() {
    document.getElementById('nav1').popPage();
}

function toCadastro() {
    document.getElementById('nav1').pushPage('cadastro.html', {
        animation: "lift"
    });
}

function toreciboRapido() {
    document.getElementById('nav1').pushPage('reciboBasico.html', {
        animation: "slide"
    });
}

function toList() {
    document.getElementById('nav1').pushPage('list.html', {
        animation: "lift"
    });
}

function toReciboBasico() {
    document.getElementById('nav1').pushPage('reciboRapido.html', {
        animation: "lift"
    });
}

function toReceptor() {
    document.getElementById('nav1').pushPage('receptor.html', {
        animation: "slide"
    });
}

function toLogin() {
    document.getElementById('nav1').pushPage('login.html', {
        animation: "lift"
    });
}
/* FUNCOES PARA NAVEGACAO NAS PAGINAS */

/*
  * retira os botoes - desabilita
  * coloca o circular
*/

function ligarAnimacao(ids, idDiv){
  for (var i=0; i< ids.length; i++){
      document.getElementById(ids[i]).disabled = true;
  }
  document.getElementById(idDiv).innerHTML = "<ons-progress-circular indeterminate></ons-progress-circular>";

}
/*
  * recoloca o botao no lugar
  * tira a animação do circulo
  * abilita novamente os botoes
*/
function desligarAnimacao(ids, idDiv, html){
  for (var i=0; i< ids.length; i++){
      document.getElementById(ids[i]).disabled = false;
  }
  document.getElementById(idDiv).innerHTML = html;
}


/* FUNCAO DE MASCARA CPF*/
function maskCPF(CPF) {
    var evt = window.event;
    kcode = evt.keyCode;
    if (kcode == 8) return;
    if (CPF.value.length == 3) {
        CPF.value = CPF.value + '.';
    }
    if (CPF.value.length == 7) {
        CPF.value = CPF.value + '.';
    }
    if (CPF.value.length == 11) {
        CPF.value = CPF.value + '-';
    }
};

/* RETIRA MASCARA CPF*/
function retiraFormatacao(cpf) {
    var value = cpf.replace(".", "");
    value = value.replace(".", "");
    value = value.replace("-", "");

    return value;
};

/* VALIDA CPF */
function validaCPF(field) {
    if (testeVazio(field)) {
        erro = new String;
        cpf = field.value;
        cpf = retiraFormatacao(cpf);
        cpf = parseInt(cpf);

        if (isNaN(cpf)) {
            erro = "Digite apenas Números!";
        }
        cpf = cpf.toString();
        if (cpf === "00000000000" ||
            cpf === "11111111111" ||
            cpf === "22222222222" ||
            cpf === "33333333333" ||
            cpf === "44444444444" ||
            cpf === "55555555555" ||
            cpf === "66666666666" ||
            cpf === "77777777777" ||
            cpf === "88888888888" ||
            cpf === "99999999999") {
            erro = "Número de CPF inválido!";
        } else if (cpf.length < 11) {
            erro = "Número de CPF com digitos faltando!"
        }
        if (erro.length > 0) {
            document.getElementById('mensagemPopover').innerHTML = erro;
            showPopover(field);

            field.focus();
            field.value = "";
        }
    }
};

/* TESTA SE O NUMERO É POSITIVO*/
function numeroPositivo(field) {
    if (testeVazio(field)) { //testa primeiramente se o campo nao esta vazio
        var numero = field.value;
        if (numero < 0 || !numero) {
            document.getElementById('mensagemPopover').innerHTML = "Digite um Numero Válido!";
            showPopover(field);
            field.focus();
            field.value = "";
        }
    }
};

/* VALIDA EMAIL */
function validaEmail(field) {
    if (testeVazio(field)) {
        usuario = field.value.substring(0, field.value.indexOf("@"));
        dominio = field.value.substring(field.value.indexOf("@") + 1, field.value.length);

        if ((usuario.length >= 1) &&
            (dominio.length >= 3) &&
            (usuario.search("@") == -1) &&
            (dominio.search("@") == -1) &&
            (usuario.search(" ") == -1) &&
            (dominio.search(" ") == -1) &&
            (dominio.search(".") != -1) &&
            (dominio.indexOf(".") >= 1) &&
            (dominio.lastIndexOf(".") < dominio.length - 1)) {} else {
            document.getElementById('mensagemPopover').innerHTML = "Email inválido";
            showPopover(field);
            field.value = "";
            field.focus();
        }
    }
};

/* VALIDA NOME*/
function validaNome(field) {
    if (testeVazio(field)) {
        nome = field.value;
        var regex = RegExp(/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\s]+$/);
        if ((regex.test(nome)) && (nome.indexOf(" ") != 0)) {

        } else {
            document.getElementById('mensagemPopover').innerHTML = "Digite um nome válido";
            showPopover(field);
            field.value = "";
            field.focus();
        }
    }
};

/*TESTA SE O CAMPO FOI PREENCHIDO*/
function testeVazio(field) {
    teste = field.value;
    if (!teste) {
        document.getElementById('mensagemPopover').innerHTML = "Este Campo não deve ficar vazio"
        //field.style.background = "pink";
        /*
        var form = document.getElementById("form");
form.addEventListener("focus", function( event ) {
  event.target.style.background = "pink";
}, true);
form.addEventListener("blur", function( event ) {
  event.target.style.background = "";
}, true);
*/
        showPopover(field);
        field.focus();
    } else {
        return true;
    }
};

/* CONSTRUTOR RECIBO LOGADO */
var reciboLogado = function() {
    var nome = document.getElementById('nomeReceptor').value;
    var email = document.getElementById('emailReceptor').value;
    var servico = document.getElementById('servico').value;
    var valor = document.getElementById('valorRecibo').value;

    this.getNome = function() {
        return nome;
    };
    this.getEmail = function() {
        return email;
    };
    this.getServico = function() {
        return servico;
    };
    this.getValor = function() {
        return valor;
    };

    this.validaReciboLogado = function() {
        if (!email || !nome || !servico || !valor) {
            return false;
        } else {
            return true;
        }
    };
}

/* CONSTRUTOR USUARIO */
var Usuario = function() {
    /* dados usuario*/
    var nome = document.getElementById('nomeUsuario').value;
    var email = document.getElementById('emailUsuario').value;
    var cpf = document.getElementById('cpf').value;
    var senha = document.getElementById('senhaUsuario').value;
    var senha2 = document.getElementById('senhaUsuario2').value;
    cpf = retiraFormatacao(cpf);
    cpf = parseInt(cpf);
    /* fim dados usuario*/

    this.getNome = function() {
        return nome;
    };
    this.getEmail = function() {
        return email;
    };
    this.getcpf = function() {
        return cpf;
    };
    this.getSenha = function() {
        return senha;
    };
    this.getSenha2 = function() {
        return senha2;
    };

    this.validaUsuario = function() {
        if (!email || !nome || !senha || !cpf || !senha2) {
            return 0;
        } else if (senha !== senha2) {
            return 1;
        } else {
            return 2;
        }
    };

}

/* CONSTRUTOR EMISSOR */
var Emissor = function() {

    var nome = document.getElementById('nomeEmissor').value;
    var email = document.getElementById('emailEmissor').value;
    var service = document.getElementById('servicoRapido').value;
    var valor = parseFloat(document.getElementById('valorRapido').value, 10); //10 == decimal

    this.getNome = function() {
        return nome;
    };
    this.getEmail = function() {
        return email;
    };
    this.getServico = function() {
        return service;
    };
    this.getValor = function() {
        return valor;
    };

    this.testeVazio = function() {
        if (!nome || !email || !service || !valor) {
            return true;
        }
        return false
    };
}

/* CONSTRUTOR RECEPTOR */
var Receptor = function() {
    /* dados receptor */
    var nome = document.getElementById('nomeReceptor').value;
    var email = document.getElementById('emailReceptor').value;

    this.getNome = function() {
        return nome;
    };
    this.getEmail = function() {
        return email;
    };

    this.testeVazio = function() {
        if (!nome || !email) {
            return true;
        } else {
            return false;
        }
    };
}


function novoEmissor() {
    emissor = new Emissor();
    var validado = emissor.testeVazio();
    if (!validado) {
        toReceptor();
    } else {
        ons.notification.alert("Certifique-se de que preencheu corretamente todos os campos!");
    }

};


/*funcao apenas fecha dialogo*/
var hideDialog = function(id) {
    document
        .getElementById(id)
        .hide();
};

/*FUNCTIONS PARA TROCA DO OLHO PARA VER A SENHA*/
function toggle_password(target) {
    var old = document.getElementById(target);
    if (old.type == 'password') {
        swapInput(old, 'text', target);

    } else {
        swapInput(old, 'password', target);
    }
}

function swapInput(old, type, target) {
    var place = "";
    if (target === 'senhaUsuario2') {
        place = 'Confirmar ';
    }
    var value = old.value;
    var btn = "<ons-input id='" + target + "' modifier='underbar' type='" + type + "' maxlength='24' placeholder='" + place + "Senha' onblur='testeVazio(this)' value='" + value + "'style='vertical-align:middle' float></ons-input>";
    var el = document.createElement('elementoNovo');
    el.innerHTML = btn;
    old.parentNode.insertBefore(el, old);
    old.parentNode.removeChild(old);
};

/* POPOVER */
var showPopover = function(target) {
    document
        .getElementById('popover')
        .show(target);
};

var hidePopover = function() {
    document
        .getElementById('popover')
        .hide();
};
