/* espera carregar (init) a pagina list.html */
document.addEventListener("init", function(event) {
    if (event.target.id == "list.html") {
        /* chama a funcao de mostrar a lista*/
        mostrarLista(document.getElementById("infinite-list"));
    }
}, false);

window.fn = {};

window.fn.open = function() {
    var menu = document.getElementById('menu');
    menu.open();
};

window.fn.load = function(page) {
    var content = document.getElementById('content');
    var menu = document.getElementById('menu');
    content.load(page)
        .then(menu.close.bind(menu));
};

/* INICIANDO SQLITE */
document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    var db = window.openDatabase("database", "1.0", "usuario", 1);
};

function createTables(){
    var query = 'CREATE TABLE IF NOT EXISTS usuario(id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, nome VARCHAR NOT NULL, email VARCHAR NOT NULL);';
    db.transaction(function(transaction){
      transaction.executeSql(query, [], nullDataHandler, errorHandler);
      updateStatus("Tabela 'usuario' status: OK.");
      });
};

function onUpdate(json){
    var id = json['id'];
    var nome = json['nome'];
    var email = json['email'];

    var query = "update usuario set nome=?, email=? where id=?;";
    localDB.transaction(function(transaction){
    transaction.executeSql(query, [nome, email, id], function(transaction, results){
      if (!results.rowsAffected) {
          updateStatus("Erro: Update não realizado.");
      }
      else {
            updateForm("", "", "");
            updateStatus("Update realizado:" + results.rowsAffected);
            queryAndUpdateOverview();
            }
      }, errorHandler);
    });
};

function onCreate(json){
    var nome = json['nome'];
    var email = json['email'];
    if (nome == "" || email == "") {
        updateStatus("Erro: 'Nome' e 'email' são campos obrigatórios!");
    }
    else {
        var query = "insert into usuario (nome, email) VALUES (?, ?);";
        try {
            localDB.transaction(function(transaction){
                transaction.executeSql(query, [nome, email], function(transaction, results){
                    if (!results.rowsAffected) {
                        updateStatus("Erro: Inserção não realizada");
                    }
                    else {
                        updateForm("", "", "");
                        updateStatus("Inserção realizada, linha id: " + results.insertId);
                        queryAndUpdateOverview();
                    }
                }, errorHandler);
            });
        }
        catch (e) {
            updateStatus("Erro: INSERT não realizado " + e + ".");
        }
    }
};


function queryAndUpdateOverview(){

 //Remove as linhas existentes para inserção das novas
   var dataRows = document.getElementById("itemData").getElementsByClassName("data");

   while (dataRows.length > 0) {
       row = dataRows[0];
       document.getElementById("itemData").removeChild(row);
   };

 //Realiza a leitura no banco e cria novas linhas na tabela.
   var query = "SELECT * FROM usuario";
   try {
       localDB.transaction(function(transaction){

           transaction.executeSql(query, [], function(transaction, results){
               for (var i = 0; i < results.rows.length; i++) {

                   var row = results.rows.item(i);
                   var li = document.createElement("li");
         li.setAttribute("id", row['id']);
                   li.setAttribute("class", "data");
                   li.setAttribute("onclick", "onSelect(this)");

                   var liText = document.createTextNode(row['nome'] + " x "+ row['idade']);
                   li.appendChild(liText);

                   document.getElementById("itemData").appendChild(li);
               }
           }, function(transaction, error){
               updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
           });
       });
   }
   catch (e) {
       updateStatus("Error: SELECT não realizado " + e + ".");
   }
}

// 3. Funções de tratamento e status.

// Tratando erros

errorHandler = function(transaction, error){
   updateStatus("Erro: " + error.message);
   return true;
}

nullDataHandler = function(transaction, results){
}

// Funções de update

function updateForm(id, nome, idade){
   document.itemForm.id.value = id;
   document.itemForm.nome.value = nome;
   document.itemForm.idade.value = idade;
}

function updateStatus(status){
   document.getElementById('status').innerHTML = status;
}

/* abre o local storage */
var storage = window.localStorage;
/* EMISSOR OBJETO GLOGAL */
var emissor = new Object();

/* funcoes para abrir novas paginas */
function pop(){
    document.getElementById('nav1').popPage();

}
function toLogado() {
    document.getElementById('nav1').pushPage('logado.html');
}

function toCadastro() {
    document.getElementById('nav1').pushPage('cadastro.html');
}

function toreciboRapido() {
    document.getElementById('nav1').pushPage('reciboBasico.html');
}

function toList() {
    document.getElementById('nav1').pushPage('list.html');
}

function toReciboBasico() {
    document.getElementById('nav1').pushPage('reciboRapido.html');
}

function toReceptor() {
    document.getElementById('nav1').pushPage('receptor.html');
}
/* funcoes para abrir novas paginas */


/* FUNCAO DE MASCARA CPF*/
function maskCPF(CPF) {
    var evt = window.event;
    kcode = evt.keyCode;
    if (kcode == 8) return;
    if (CPF.value.length == 3) {
        CPF.value = CPF.value + '.';
    }
    if (CPF.value.length == 7) {
        CPF.value = CPF.value + '.';
    }
    if (CPF.value.length == 11) {
        CPF.value = CPF.value + '-';
    }
}

/* RETIRA MASCARA CPF*/
function retiraFormatacao(cpf) {
    var value = cpf.replace(".", "");
    value = value.replace(".", "");
    value = value.replace("-", "");

    return value;
}

/* VALIDA CPF */
function validaCPF(field) {
  if(testeVazio(field)){
    erro = new String;
    cpf = field.value;
    cpf = retiraFormatacao(cpf);
    cpf = parseInt(cpf);

    if (isNaN(cpf)) {
      erro = "Digite apenas Números!";
    }
    cpf = cpf.toString();
    if (cpf === "00000000000" ||
      cpf === "11111111111" ||
      cpf === "22222222222" ||
      cpf === "33333333333" ||
      cpf === "44444444444" ||
      cpf === "55555555555" ||
      cpf === "66666666666" ||
      cpf === "77777777777" ||
      cpf === "88888888888" ||
      cpf === "99999999999") {
      erro = "Número de CPF inválido!";
    }else if (cpf.length < 11) {
      erro = "Número de CPF com digitos faltando!"
    }
    if (erro.length > 0) {
      document.getElementById('mensagemPopover').innerHTML=erro;
      showPopover(field);

      field.focus();
      field.value = "";
    }
  }
}

/* TESTA SE O NUMERO É POSITIVO*/
function numeroPositivo(field) {
    if(testeVazio(field)){ //testa primeiramente se o campo nao esta vazio
      var numero = field.value;
      if (numero < 0 || !numero) {
        document.getElementById('mensagemPopover').innerHTML="Digite um Numero Válido!";
        showPopover(field);
          field.focus();
          field.value= "";
      }
    }
}

/* VALIDA EMAIL */
function validaEmail(field) {
    if(testeVazio(field)){
      usuario = field.value.substring(0, field.value.indexOf("@"));
      dominio = field.value.substring(field.value.indexOf("@") + 1, field.value.length);

      if ((usuario.length >= 1) &&
          (dominio.length >= 3) &&
          (usuario.search("@") == -1) &&
          (dominio.search("@") == -1) &&
          (usuario.search(" ") == -1) &&
          (dominio.search(" ") == -1) &&
          (dominio.search(".") != -1) &&
          (dominio.indexOf(".") >= 1) &&
          (dominio.lastIndexOf(".") < dominio.length - 1)) {} else {
          document.getElementById('mensagemPopover').innerHTML="Email inválido";
          showPopover(field);
          field.value = "";
          field.focus();
      }
    }
}

/* VALIDA NOME*/
function validaNome(field) {
    if(testeVazio(field)){
      nome = field.value;
      var regex = RegExp(/^[A-Za-záàâãéèêíïóôõöúçñÁÀÂÃÉÈÍÏÓÔÕÖÚÇÑ'\s]+$/);
      if ((regex.test(nome)) && (nome.indexOf(" ") != 0)) {

      } else {
        document.getElementById('mensagemPopover').innerHTML="Digite um nome válido";
        showPopover(field);
          field.value = "";
          field.focus();
      }
  }
}

/*TESTA SE O CAMPO FOI PREENCHIDO*/
function testeVazio(field) {
    teste = field.value;
    if (!teste) {
        document.getElementById('mensagemPopover').innerHTML="Este Campo não deve ficar vazio"
        showPopover(field);
        field.focus();
    }else{
      return true;
    }
}

/* CONSTRUTOR USUARIO */
var Usuario = function() {
    /* dados usuario*/
    var nome = document.getElementById('nomeUsuario').value;
    var email = document.getElementById('emailUsuario').value;
    var cpf = document.getElementById('cpf').value;
    var senha = document.getElementById('senhaUsuario').value;
    var senha2 = document.getElementById('senhaUsuario2').value;
    cpf = retiraFormatacao(cpf);
    cpf = parseInt(cpf);
    /* fim dados usuario*/

    this.getNome = function() {
        return nome;
    };
    this.getEmail = function() {
        return email;
    };
    this.getcpf = function() {
        return cpf;
    };
    this.getSenha = function() {
        return senha;
    };
    this.getSenha2 = function() {
        return senha2;
    };

    this.validaUsuario = function(){
      if(!email || !nome || !senha || !cpf || !senha2){
          return 0;
      }else if(senha !== senha2){
        return 1;
      }else {
        return 2;
      }
    };

}

/* CONSTRUTOR EMISSOR */
var Emissor = function() {
    /* dados emissor */
    var nome = document.getElementById('nomeEmissor').value;
    var email = document.getElementById('emailEmissor').value;
    var service = document.getElementById('servicoRapido').value;
    var valor = parseFloat(document.getElementById('valorRapido').value, 10); //10 == decimal

    this.getNome = function() {
        return nome;
    };
    this.getEmail = function() {
        return email;
    };
    this.getService = function() {
        return service;
    };
    this.getValor = function() {
        return valor;
    };

    this.testeVazio = function() {
        if (!nome || !email || !service || !valor) {
            return true;
        }
        return false
    };
}

/* CONSTRUTOR RECEPTOR */
var Receptor = function() {
    /* dados receptor */
    var nome = document.getElementById('nomeReceptor').value;
    var email = document.getElementById('emailReceptor').value;

    this.getNome = function() {
        return nome;
    };
    this.getEmail = function() {
        return email;
    };

    this.testeVazio = function() {
        if (!nome || !email) {
            return true;
        }else{
          return false;
        }
    };
}

/* CONTRUTOR RECIBO */
var Recibo = function(emissor, receptor) {
    var receptor = receptor;
    var now = new Date;
    this.nomeEmissor = emissor.getNome();
    this.nomeReceptor = receptor.getNome();
    this.emailEmissor = emissor.getEmail();
    this.emailReceptor = receptor.getEmail();
    this.valor = emissor.getValor();
    this.service = emissor.getService();
    this.dia = now.getDate();
    this.mes = now.getMonth();
    this.ano = now.getFullYear();

}

/* cria de fato a lista infinita*/
function mostrarLista(infiniteList) {
    infiniteList.delegate = {
        createItemContent: function(i) {
            var valueRecibo = JSON.parse(storage.getItem("recibo" + i));
            return ons._util.createElement(
                '<ons-list-item tappable="true" onclick="mostrarDados(' + i + ')"> ' + (i + 1) + ' ' + valueRecibo.service +
                ' - para ' + valueRecibo.nomeReceptor + '&nbsp;&nbsp;<i class="fa fa-eye" aria-hidden="true"></i></ons-list-item>'
            );
        },
        countItems: function() {
            return ((storage.length));
        },
        calculateItemHeight: function() {
            return ons.platform.isAndroid() ? 48 : 44;
        }
    };
}

/* FUNÇÕES STORAGE */
function mostrarDados(i) {
    var reciboAtual = JSON.parse(storage.getItem("recibo" + i));
    alert("Nome do Emissor: " + reciboAtual.nomeEmissor + "\nEmail do Emissor: " + reciboAtual.emailEmissor +
        "\nNome do Receptor: " + reciboAtual.nomeReceptor + "\nEmail do Receptor: " + reciboAtual.emailReceptor +
        "\nServiço: " + reciboAtual.service + "\nValor: R$" + reciboAtual.valor + "\nData: " + reciboAtual.dia + "/" + reciboAtual.mes + "/" + reciboAtual.ano);
}

/* adiciona o recibo em questao no storage */
function addStorage(text) {
    var key = "recibo" + storage.length;
    storage.setItem(key, text);
}

function removeStorage() {

}
/* FIM FUNÇÕES STORAGE */

function novoEmissor() {
    emissor = new Emissor();
    var validado = emissor.testeVazio();
    if (!validado) {
        toReceptor();
    } else {
        ons.notification.alert("Certifique-se de que preencheu corretamente todos os campos!");
    }

}

function novoRecibo() {
    var receptor = new Receptor();
    var recibo = new Recibo(emissor, receptor);
    var validado = receptor.testeVazio();
    if (!validado) {
        /* caso preencher os campos */
        document.getElementById('nomeEmissor').value = "";
        document.getElementById('emailEmissor').value = "";
        document.getElementById('servicoRapido').value = "";
        document.getElementById('valorRapido').value = "";
        document.getElementById('nomeReceptor').value = "";
        document.getElementById('emailReceptor').value = "";
        var text = JSON.stringify(recibo);
        addStorage(text);
        confirm("Recibo gerado com sucesso.");
        pop(); //volta a pagina
    } else {
        ons.notification.alert("Certifique-se de que preencheu corretamente todos os campos!");
    }
}

function cadastrar(){
  var usuario = new Usuario();
  var validado = usuario.validaUsuario();
  switch (validado) {
    case 0:
      ons.notification.alert("Certifique-se de que preencher corretamente todos os campos!");
      break;
    case 1:
      ons.notification.alert("As senhas não coincidem!");
      document.getElementById('senhaUsuario').value = "";
      document.getElementById('senhaUsuario2').value= "";
      break;
    default:
     /* CADASTRAR AQ*/
     document.getElementById('esperaCadastro').innerHTML = "<ons-progress-circular indeterminate></ons-progress-circular>";
     document.getElementById('btn-back-cadastro').disabled = true;
     xhr = new XMLHttpRequest();
     var url =  "http://192.168.10.112/3s/recibos3s/web/communication/?json="+encodeURIComponent(JSON.stringify({"token":"123","action":"register","email":usuario.getEmail(),"senha":usuario.getSenha(),"cpf":usuario.getcpf(), "nome":usuario.getNome()}));
     xhr.open("GET", url, true);
     xhr.setRequestHeader("Content-type", "application/json");
     xhr.onreadystatechange = function () {
       if (xhr.readyState == 4 && xhr.status == 200) {
         var json = JSON.parse(xhr.responseText);

         if(json['retorno']){
           /* sincronizar banco sqlite*/
           /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
            document.getElementById('btn-back-cadastro').disabled = true;
           document.getElementById('espera').innerHTML = "<ons-button id='btn-login' onclick='login()'><ons-ripple></ons-ripple> Entrar </ons-button>"
            ons.notification.alert(json['mensagem']);
            toLogado();
         }else{
           /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
            document.getElementById('btn-back-cadastro').disabled = true;
           document.getElementById('esperaCadastro').innerHTML = "<ons-button id='esperaCadastro' onclick='cadastrar()' class='button'><ons-ripple></ons-ripple> Cadastrar</ons-button>"
          ons.notification.alert(json['mensagem']);
         }
       }
     };
     xhr.timeout = 10000;
     xhr.ontimeout = function (){
       /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
        document.getElementById('btn-back-cadastro').disabled = true;
       document.getElementById('esperaCadastro').innerHTML = "<ons-button id='esperaCadastro' onclick='cadastrar()' class='button'><ons-ripple></ons-ripple> Cadastrar</ons-button>"
       ons.notification.alert ("Não conseguimos estabelecer uma conexão com o servidor!");
     };

     xhr.send();
  }

}

/* FUNCAO DE LOGIN */
function login() {
  var email = document.getElementById('emailUsuarioLogin').value;
  var senha = document.getElementById('senhaUsuarioLogin').value;
  if(!senha || !email){
    ons.notification.alert("Certifique-se de que preencheu corretamente todos os campos!");
  }else{
    document.getElementById('espera').innerHTML = "<ons-progress-circular indeterminate></ons-progress-circular>";
    document.getElementById('btn-inscrevase').disabled=true;
    document.getElementById('btn-gerarecibo').disabled=true;
    xhr = new XMLHttpRequest();
    var url =  "http://192.168.10.112/3s/recibos3s/web/communication/?json="+encodeURIComponent(JSON.stringify({"token":"123","action":"login","email":email,"senha":senha}));
    xhr.open("GET", url, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200) {
        var json = JSON.parse(xhr.responseText);
        if(json['retorno']){
          /* sincronizar banco sqlite*/
          /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
            document.getElementById('btn-inscrevase').disabled=false;
            document.getElementById('btn-gerarecibo').disabled=false;
            document.getElementById('espera').innerHTML = "<ons-button id='btn-login' onclick='login()'><ons-ripple></ons-ripple> Entrar </ons-button>"
            /*muda para a pagina de login*/
            toLogado();
        }else{
          /*recoloca o botao no lugar - tira a animação do circulo e abilita novamente os botoes*/
            document.getElementById('btn-inscrevase').disabled=false;
            document.getElementById('btn-gerarecibo').disabled=false;
            document.getElementById('espera').innerHTML = "<ons-button id='btn-login' onclick='login()'><ons-ripple></ons-ripple> Entrar </ons-button>"
            ons.notification.alert(json['mensagem']);
        }
      }
    };
    xhr.timeout = 10000;
    xhr.ontimeout = function (){
      document.getElementById('btn-inscrevase').disabled=false;
      document.getElementById('btn-gerarecibo').disabled=false;
      document.getElementById('espera').innerHTML = "<ons-button id='btn-login' onclick='login()'><ons-ripple></ons-ripple> Entrar </ons-button>"
      ons.notification.alert ("Não conseguimos estabelecer uma conexão com o servidor!");
    };
    xhr.send();}
  }
/* FIM FUNCAO DE LOGIN */
function logout(){
  document.getElementById('nav1').onDeviceBackButton.destroy(); // Destroys the current handler
  pop();
}

function toggle_password(target){
    var old = document.getElementById(target);
    if (old.type == 'password'){
        swapInput(old, 'text', target);

    }else {
        swapInput(old, 'password', target);
    }
}

function swapInput(old, type, target) {
  var place="";
  if(target === 'senhaUsuario2'){
    place = 'Confirmar ';
  }
  var value = old.value;
  var btn = "<ons-input id='"+target+"' modifier='underbar' type='"+type+"' maxlength='24' placeholder='"+place+"Senha' onblur='testeVazio(this)' value='"+value+"' float></ons-input>";
  var el = document.createElement('naoInteressa');
  el.innerHTML = btn;
  old.parentNode.insertBefore(el, old);
  old.parentNode.removeChild(old);
  }

/* POPOVER */
var showPopover = function(target) {
  document
    .getElementById('popover')
    .show(target);
};

var hidePopover = function() {
  document
    .getElementById('popover')
    .hide();
};
